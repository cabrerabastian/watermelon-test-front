
import { messageContants } from '../constants';

export const messageAction = {
    success,
    error,
    clear
};

function success(message) {
    return { type: messageContants.SUCCESS, message };
}

function error(message) {
    return { type: messageContants.ERROR, message };
}
function clear() {
    return { type: messageContants.CLEAR };
}