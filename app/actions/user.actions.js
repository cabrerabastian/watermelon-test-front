import { userService } from '../services';
import { messageAction } from './';
import { userConstants } from '../constants';
import cookie from 'react-cookies'

export const userActions = {
  login,
  logout,
  validateToken,
  checkCookie
};

function login(email, password) {
  return dispatch => {
    dispatch(request({ email }));
    userService.login(email, password)
      .then(
        userWithData => {
          if (userWithData.error) {
            dispatch(messageAction.error(userWithData.message));
            dispatch(failure(userWithData))
          } else {
            cookie.save('token', userWithData.token, { path: '/' })
            dispatch(success(userWithData));
          }

          // history.push('/');
        },
        error => {
          dispatch(failure(error));
          dispatch(messageAction.error(error));
        }
      );
  };

  function request(data) { return { type: userConstants.LOGIN_REQUEST, data } }
  function success(data) { return { type: userConstants.LOGIN_SUCCESS, data } }
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}
function logout() {
  userService.logout();
  return { type: userConstants.LOGOUT };
}
function validateToken() {
  const token = cookie.load('token');
  return dispatch => {
    dispatch(request({}));
    userService.validateToken(token)
      .then(
        data => {
          if (data.error) {
            cookie.remove('token', { path: '/' })
            dispatch(failure(data))
          } else {
            dispatch(success(data));
          }

          // history.push('/');
        },
        error => {
          dispatch(failure(error));
        }
      );
  };

  function request(data) { return { type: userConstants.VALIDATE_REQUEST, data } }
  function success(data) { return { type: userConstants.VALIDATE_SUCCESS, data } }
  function failure(error) { return { type: userConstants.VALIDATE_FAIL, error } }
}

function checkCookie() {
  const token = cookie.load('token');
  return dispatch => {
    dispatch(request({}));
    if (token) {
      dispatch(success())
    } else {
      dispatch(failure())
    }
  };

  function request() { return { type: userConstants.COOKIE_REQUEST } }
  function success() { return { type: userConstants.COOKIE_SUCCESS } }
  function failure() { return { type: userConstants.COOKIE_FAIL } }
}