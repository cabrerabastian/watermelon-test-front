import React from 'react'
import Character from '../components/Character';
import CharacterList from '../components/CharacterList';

import { shallow } from '../../enzyme';
const characters = require('../../__mockData__/characters.json');

describe('components', () => {
  describe('Character', () => {
    it('should render Character', () => {
      const wrapper = shallow(<Character character={characters[0]} />);
      // expect(wrapper.debug()).toMatchSnapshot();
      expect(wrapper.find('CardTitle').text()).toEqual(characters[0].name)
      expect(wrapper.find('CardText').text().includes(characters[0].status)).toBe(true);
      expect(wrapper.find('CardText').text().includes(characters[0].status)).toBe(true);
      expect(wrapper.find('CardText').text().includes(characters[0].species)).toBe(true);
      expect(wrapper.find('CardImg').prop("src")).toEqual(characters[0].image)
    })
  })
  describe('CharacterList', () => {
    it('should render self and subcomponent Character', () => {
      const wrapper = shallow(<CharacterList dataRickAndMorty={characters} />);
      // expect(wrapper.debug()).toMatchSnapshot();
      expect(wrapper.find('Character').length).toEqual(characters.length)
    })
  })
})