const apiUrl = `http://localhost:5000`;

export const userService = {
  login,
  logout,
  validateToken
};


function login(email, password) {

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*" },
    body: JSON.stringify({ email, password })
  };

  return fetch(`${apiUrl}/users/login`, requestOptions)
    .then((result) => result.json())
    .then(data => {
      return data;
    });
}
function logout() {
  cookie.remove('token', { path: '/' })
}

function validateToken(token) {

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', "Access-Control-Allow-Origin": "*", "Authorization": token },
    body: JSON.stringify({})
  };

  return fetch(`${apiUrl}/validate/validateToken`, requestOptions)
    .then((result) => result.json())
    .then(data => {
      return data;
    });
}