import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import cookie from 'react-cookies'
import { userActions } from '../actions';
import { connect } from 'react-redux';
import { history } from '../helpers';

class Login extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    email: '',
    password: '',
  }
  componentDidMount() {
    if (this.props) {
      this.props.checkCookie();
    }
  }
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;
    if (email && password) {
      this.props.login(email, password);
    }
  }
  render() {
    const messageData = this.props.message;
    const userData = this.props.user;
    if (userData.redirect || userData.redirectList) {
      return <Redirect to={{
        pathname: '/list',
        // state: { dataRickAndMorty: this.dataRickAndMorty }
      }} />
    }

    return <div style={{ width: "100%" }}>
      <Form onSubmit={this.handleSubmit} style={{ "margin": "50px" }}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control required type="email" name="email" placeholder="Ingrese Email" onChange={this.handleChange} />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Contraseña</Form.Label>
          <Form.Control required type="password" name="password" placeholder="Ingrese Contraseña" onChange={this.handleChange} />
        </Form.Group>
        <Button variant="primary" type="submit" style={{ "marginBottom": "20px" }}>
          Entrar
        </Button>
        {messageData.message && <Alert variant="danger">
          {messageData.message}
        </Alert>}
      </Form>
    </div>
  }
}
function mapState(state) {
  const { message, user } = state;
  return { message, user };
}
const actionCreators = {
  login: userActions.login,
  checkCookie: userActions.checkCookie
};

export default connect(mapState, actionCreators)(Login);