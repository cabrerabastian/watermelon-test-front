import React, { PropTypes } from 'react';
import Character from './Character';


const CharacterList = ({dataRickAndMorty}) => {
  return (
    dataRickAndMorty ? dataRickAndMorty.map(character => <Character key={character.name} character={character} />) : <div></div>
  );
}

// CharacterList.propTypes = {
//   data: PropTypes.arrayOf(PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     status: PropTypes.string.isRequired,
//     gender: PropTypes.string.isRequired,
//     species: PropTypes.string.isRequired,
//     image: PropTypes.string.isRequired
//   }).isRequired).isRequired
// }

export default CharacterList;