import React, { PropTypes } from 'react'
import Card from 'react-bootstrap/Card';

const Character = ({character}) => {
  return (
    <Card style={{ width: '18rem',  margin: '10px' }}>
      <Card.Img variant="top" src={character.image} />
      <Card.Body>
        <Card.Title>{character.name}</Card.Title>
        <Card.Text>
          Status: {character.status} <br />
          Species: {character.species} <br />
          Gender: {character.gender}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}
// Character.propTypes = {
//   character: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     status: PropTypes.string.isRequired,
//     gender: PropTypes.string.isRequired,
//     species: PropTypes.string.isRequired,
//     image: PropTypes.string.isRequired
//   }).isRequired,
// }
export default Character;