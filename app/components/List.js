import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { Redirect } from 'react-router-dom'
import { userActions } from '../actions';
import { connect } from 'react-redux';
import CharacterList from './../components/CharacterList'

class List extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    if (this.props) {
      this.props.validate();
    }
  }
  render() {
    const userData = this.props.user;
    if (userData && userData.redirectHome) {
      console.log('going to home!');
      //TODO: need remove cookie
      // redirect to home if signed up
      return <Redirect to={{ pathname: "/login" }} />;
    }
    return <Container fluid>
      <Row className="justify-content-md-center">
        <CharacterList dataRickAndMorty={userData.dataRickAndMorty} />
      </Row>
    </Container>

  }
}
function mapState(state) {
  const { message, user } = state;
  return { message, user };
}
const actionCreators = {
  validate: userActions.validateToken,
};

export default connect(mapState, actionCreators)(List);