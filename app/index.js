import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { store } from './helpers';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Link, BrowserRouter as Router, Redirect } from 'react-router-dom'
import Login from "./components/Login";
import List from "./components/List";



const rootElement = document.getElementById("app");
ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Route exact path="/" component={Login} />
        <Route path="/login" component={Login} />
        <Route path="/list" component={List} />
        <Redirect from="*" to="/login" />
      </div>
    </Router>
  </Provider>,
  rootElement
);