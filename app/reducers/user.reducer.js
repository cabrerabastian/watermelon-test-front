import { userConstants } from '../constants';


export function user(state = {}, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {};
    case userConstants.LOGIN_SUCCESS:
      return {
        redirect: true,
        dataRickAndMorty: action.data.dataRickAndMorty
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.VALIDATE_FAIL:
      return {
        redirectHome: true
      };
    case userConstants.VALIDATE_REQUEST:
      return {
      };
    case userConstants.VALIDATE_SUCCESS:
      return {
        dataRickAndMorty: action.data.dataRickAndMorty
      };
    case userConstants.COOKIE_REQUEST:
      return {
      };
    case userConstants.COOKIE_FAIL:
      return {
      };
    case userConstants.COOKIE_SUCCESS:
      return {
        redirectList: true
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}