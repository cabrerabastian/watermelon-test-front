import { messageContants} from '../constants';

export function message(state = {}, action) {
  switch (action.type) {
    case messageContants.SUCCESS:
      return {
        type: 'alert-success',
        message: action.message
      };
    case messageContants.ERROR:
      return {
        type: 'alert-danger',
        message: action.message
      };
    case messageContants.CLEAR:
      return {};
    default:
      return state
  }
}