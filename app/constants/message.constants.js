export const messageContants = {
  SUCCESS: 'MESSAGE_SUCCESS',
  ERROR: 'MESSAGE_ERROR',
  CLEAR: 'MESSAGE_CLEAR'
};
